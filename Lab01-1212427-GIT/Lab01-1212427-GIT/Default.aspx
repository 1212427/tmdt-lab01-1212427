﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Lab01_1212427_GIT.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Thông tin sinh viên</title>
    <style>
        td{
            text-align: center;
            vertical-align: central;
            font-size: 18px;
        }
        .proptd{
            font-weight: bold;
            color: firebrick;
            background-color: wheat;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="600" height="150" border="1" align="center">
        <tr><th colspan="3" style="font-family:'Segoe UI'; font-size: 21px; color: white; background-color: orangered">THÔNG TIN SINH VIÊN</th></tr>
        <tr>
            <td class="proptd">MSSV</td>
            <td>1212427</td>
            <td rowspan="3"><img src="HoTheTong.jpg"/></td>
        </tr>
        <tr>
            <td class="proptd">Họ Tên</td>
            <td>HỒ THẾ TÔNG</td>
        </tr>
        <tr>
            <td class="proptd">Email</td>
            <td><a href="">1212427@student.hcmus.edu.vn</a></td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
